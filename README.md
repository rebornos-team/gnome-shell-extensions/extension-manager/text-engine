# text-engine

A lightweight rich-text framework for GTK

https://github.com/mjakeman/text-engine

<br>

How to clone this repository:
```
git clone https://gitlab.com/rebornos-team/gnome-shell-extensions/extension-manager/text-engine.git
```

